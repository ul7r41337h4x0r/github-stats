/*

    github-stats Copyright (C) 2018  scott snyder

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package main

import (
	"github.com/google/go-github/github"
	"golang.org/x/oauth2"

	"context"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	version = "v0.06a"
)

var (
	totalclones   int
	totalviews    int
	totalstars    int
	totalforks    int
	totalissues   int
	totalwatchers int
	totaldownct   int
	colors        bool
	all           bool
	allrel        bool
	latrel        bool
	user          string
	token         string
	reponame      string
)

func color(input, color string) string {
	var output string
	if color == "black" {
		output = fmt.Sprintf("\033[0;30m" + input + "\033[0m")
	}
	if color == "red" {
		output = fmt.Sprintf("\033[0;31m" + input + "\033[0m")
	}
	if color == "green" {
		output = fmt.Sprintf("\033[0;32m" + input + "\033[0m")
	}
	if color == "orange" {
		output = fmt.Sprintf("\033[0;33m" + input + "\033[0m")
	}
	if color == "blue" {
		output = fmt.Sprintf("\033[0;34m" + input + "\033[0m")
	}
	if color == "purple" {
		output = fmt.Sprintf("\033[0;35m" + input + "\033[0m")
	}
	if color == "cyan" {
		output = fmt.Sprintf("\033[0;36m" + input + "\033[0m")
	}
	if color == "lightgrey" {
		output = fmt.Sprintf("\033[0;37m" + input + "\033[0m")
	}
	if color == "darkgray" {
		output = fmt.Sprintf("\033[1;30m" + input + "\033[0m")
	}
	if color == "lightred" {
		output = fmt.Sprintf("\033[1;31m" + input + "\033[0m")
	}
	if color == "lightgreen" {
		output = fmt.Sprintf("\033[1;32m" + input + "\033[0m")
	}
	if color == "yellow" {
		output = fmt.Sprintf("\033[1;33m" + input + "\033[0m")
	}
	if color == "lightblue" {
		output = fmt.Sprintf("\033[1;34m" + input + "\033[0m")
	}
	if color == "lightpurple" {
		output = fmt.Sprintf("\033[1;35m" + input + "\033[0m")
	}
	if color == "lightcyan" {
		output = fmt.Sprintf("\033[1;36m" + input + "\033[0m")
	}
	if color == "white" {
		output = fmt.Sprintf("\033[1;37m" + input + "\033[0m")
	}
	return output
}

// Slowly print 1 char at a time with user defined delay between char prints,
// accepts newlines
func slowly(inputtext string, delay int) {
	passes := len(inputtext)
	for i := 0; i <= passes; i++ {
		fmt.Printf("%s", string(inputtext[i]))
		time.Sleep(time.Duration(delay) * time.Millisecond)
		if i == (passes - 1) {
			break
		}
	}
}

func help(command string) {
	if colors {
		slowly(color("github-stats "+version+" \n", "red"), 10)
		slowly(color("Command: "+command+` {args}
                --all                 List All Repos
                --colors              Output Color(default:false)
                --repo string         Github Repo Name(required, if '--all' is not declared) 
                --token string        Github Access Token(required)
                --user string         Github Username(required)
                --all-releases        list release info of all releases
                --latest-release      list release info of latest release
            `, "cyan"), 10)
		slowly(color("  nw-python-dev@protonmail.com\nhttps://bitbucket.org/ul7r41337h4x0r/github-stats\n", "blue"), 10)
		os.Exit(2)
	} else {
		slowly("github-stats "+version+" \n", 10)
		slowly("Command: "+command+` {args}
                --all                 List All Repos
                --colors              Output Color(default:false)
                --repo string         Github Repo Name(required, if '--all' is not declared)
                --token string        Github Access Token(required)
                --user string         Github Username(required)
                --all-releases        list release info of all releases
                --latest-release      list release info of latest release
`, 10)
		slowly("  nw-python-dev@protonmail.com\nhttps://bitbucket.org/ul7r41337h4x0r/github-stats\n", 10)
		os.Exit(2)
	}
}

func main() {
	flag.StringVar(&user, "user", "", "github username(required)")
	flag.StringVar(&token, "token", "", "github access token(required)")
	flag.StringVar(&reponame, "repo", "", "github repo name")
	flag.BoolVar(&all, "all", false, "list all repos of user")
	flag.BoolVar(&allrel, "all-releases", false, "list release info of all releases")
	flag.BoolVar(&latrel, "latest-release", false, "list release info of latest release")
	flag.BoolVar(&colors, "colors", false, "output color(Default: false)")
	flag.Parse()
	com := os.Args[0]
	comm := strings.Split(com, "/")
	command := comm[len(comm)-1]
	if len(os.Args) < 3 {
		help(command)
	}
	ctx := context.Background()
	oauth := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)
	tc := oauth2.NewClient(ctx, oauth)
	client := github.NewClient(tc)
	repos, _, _ := client.Repositories.List(ctx, user, &github.RepositoryListOptions{Type: "owner", Sort: "updated", Direction: "desc"})
	/*
		if err != nil {
			fmt.Println("could not get repo list")
		}
	*/
	statstr := "Showing Stats of " + user + "'s Github"
	if colors {
		fmt.Println(color(statstr, "lightblue"))
	} else {
		fmt.Println(statstr)
	}
	for _, r := range repos {
		if all {
			reponame = *r.Name
		}
		releases, _, _ := client.Repositories.ListReleases(
			ctx, user, reponame, nil)
		/*
			if err != nil {
				fmt.Println("could not get releases")
			}
		*/
		totaldown := 0
		for _, r := range releases {
			for _, a := range r.Assets {
				totaldown += a.GetDownloadCount()
			}
		}
		views, _, _ := client.Repositories.ListTrafficViews(ctx, user, reponame, &github.TrafficBreakdownOptions{Per: "week"})
		/*
			if err != nil {
				fmt.Println("could not get Traffic Views: You do not own this repo")
			}
		*/
		clones, _, _ := client.Repositories.ListTrafficClones(ctx, user, reponame, &github.TrafficBreakdownOptions{Per: "week"})
		/*
			if err != nil {
				fmt.Println("could not get Traffic Clones: You do not own this repo")
			}
		*/
		langs, _, _ := client.Repositories.ListLanguages(ctx, user, reponame)
		/*
			if err != nil {
				fmt.Println("could not get langs")
			}
		*/
		var langstr string
		langspace := "    lang: "
		langstr = langstr + fmt.Sprintf("%s", langspace)
		for l1, l2 := range langs {
			langstr = langstr + fmt.Sprintf(""+l1)
			langstr = langstr + fmt.Sprintf(":"+strconv.Itoa(l2)+" ")
		}
		if len(langstr) < 12 {
			langstr = langspace + "Repo has no Lang"
		}
		starsstr := "    Stars: " + strconv.Itoa(*r.StargazersCount)
		forksstr := "    Forks: " + strconv.Itoa(*r.ForksCount)
		viewstr := "    Unique View Count: " + strconv.Itoa(views.GetUniques())
		clonestr := "    Unique Clones: " + strconv.Itoa(clones.GetUniques())
		reldl := "Release Downloads: " + strconv.Itoa(totaldown)
		issuesstr := "    Open Issues: " + strconv.Itoa(*r.OpenIssuesCount)
		watchersstr := "    Watchers: " + strconv.Itoa(*r.WatchersCount)
		if colors {
			fmt.Println(color(reponame, "red"))
			fmt.Println(color(langstr, "green"))
			fmt.Println(color(starsstr, "lightred"))
			fmt.Println(color(forksstr, "orange"))
			fmt.Println(color(viewstr, "cyan"))
			fmt.Println(color(clonestr, "lightpurple"))
			fmt.Println(color(issuesstr, "blue"))
			fmt.Println(color(watchersstr, "lightgreen"))
		} else {
			fmt.Println(reponame)
			fmt.Println(langstr)
			fmt.Println(starsstr)
			fmt.Println(forksstr)
			fmt.Println(viewstr)
			fmt.Println(clonestr)
			fmt.Println(issuesstr)
			fmt.Println(watchersstr)
		}
		totalclones = totalclones + clones.GetUniques()
		totalviews = totalviews + views.GetUniques()
		totalforks = totalforks + *r.ForksCount
		totalissues = totalissues + *r.OpenIssuesCount
		totalstars = totalstars + *r.StargazersCount
		totalwatchers = totalwatchers + *r.WatchersCount
		totaldownct = totaldownct + totaldown
		if latrel {
			reli, _, _ := client.Repositories.GetLatestRelease(ctx, user, reponame)
			/*
				if err != nil {
					fmt.Println("could not get release info")
				}
			*/
			if len(reli.GetTagName()) > 1 {
				fmt.Println("    Latest Release:")
				fmt.Printf("        Release Name: %s\n", reli.GetName())
				fmt.Printf("        Version Tag: %s\n", reli.GetTagName())
				fmt.Printf("        Time of Release: %s\n", reli.GetPublishedAt())
				fmt.Printf("        %s\n", reldl)
			}
		}
		if allrel {
			for _, r := range releases {
				fmt.Printf("    Release Name: %v\n", *r.Name)
				fmt.Printf("        Version Tag: %v\n", *r.TagName)
				fmt.Printf("        Time of Release: %s\n", r.GetPublishedAt())
			}
			if len(releases) > 0 {
				fmt.Printf("    total release downloads: %s\n", reldl)
			}
		}
		if all == false {
			break
		}
	}
	if all {
		fmt.Println("Totals:")
		fmt.Println("  Total Watchers: " + strconv.Itoa(totalwatchers))
		fmt.Println("  Total Forks: " + strconv.Itoa(totalforks))
		fmt.Println("  Total Open Issues: " + strconv.Itoa(totalissues))
		fmt.Println("  Total Stars: " + strconv.Itoa(totalstars))
		fmt.Println("  Total Unique Views: " + strconv.Itoa(totalviews))
		fmt.Println("  Total Unique Clones: " + strconv.Itoa(totalclones))
		fmt.Println("  Total Release Downloads: " + strconv.Itoa(totaldownct))
	}
	os.Exit(0)
}
